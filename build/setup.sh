#!/bin/sh

# Setup database
mkdir -p /var/run/postgresql/9.6-main.pg_stat_tmp
sed -r -i.bak 's/^(local[ ]+all[ ]+all[ ]+)peer$/\1 md5/g' /etc/postgresql/9.6/main/pg_hba.conf
echo "listen_addresses='*'" >> /etc/postgresql/9.6/main/postgresql.conf
/etc/init.d/postgresql start
/usr/lib/postgresql/9.6/bin/postgres -D /var/lib/postgresql/9.6/main -c config_file=/etc/postgresql/9.6/main/postgresql.conf
psql -c "create database test_project;"
psql -c "create user test_project password 'test_project'"
psql -c "GRANT ALL PRIVILEGES ON DATABASE test_project TO test_project;"
psql -d test_project -f /var/lib/postgresql/schema.sql
/etc/init.d/postgresql stop
#/sbin/my_init
/usr/lib/postgresql/9.6/bin/postgres -D /var/lib/postgresql/9.6/main -c config_file=/etc/postgresql/9.6/main/postgresql.conf
