FROM phusion/baseimage:latest

# Install Postgresql
RUN echo 'deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main' | tee /etc/apt/sources.list.d/pgdg.list
RUN curl https://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc -o /tmp/ACCC4CF8.asc
RUN apt-key add /tmp/ACCC4CF8.asc
RUN apt-get update
RUN apt-get install postgresql-9.6 postgresql-client-9.6 postgresql-contrib-9.6 -y

# Moving files
ADD build/django_migrations.pg /var/lib/postgresql/django_migrations.pg
ADD build/schema.sql /var/lib/postgresql/schema.sql

# Setup Database
RUN  /etc/init.d/postgresql start &&\
    su postgres -c "psql --command \"CREATE USER project with password 'project';\" " &&\
    su postgres -c "psql --command \"CREATE DATABASE project with owner project;\" " &&\
    su postgres -c "psql --command \"CREATE EXTENSION IF NOT EXISTS hstore;\"" &&\
    su postgres -c "psql -d project -f /var/lib/postgresql/schema.sql" &&\
    su postgres -c "psql project < /var/lib/postgresql/django_migrations.pg"

# Configuring Database
RUN sed -r -i.bak 's/^(local[ ]+all[ ]+all[ ]+)peer$/\1 md5/g' /etc/postgresql/9.6/main/pg_hba.conf
RUN echo "host all all 0.0.0.0/0 trust" >> /etc/postgresql/9.6/main/pg_hba.conf
RUN echo "listen_addresses='*'" >> /etc/postgresql/9.6/main/postgresql.conf

# END Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

USER postgres

# How backup table
# RUN pg_dump --data-only --table=django_migrations project > djangomigrations.pg

EXPOSE 5432

CMD ["/usr/lib/postgresql/9.6/bin/postgres", "-D", "/var/lib/postgresql/9.6/main", "-c", "config_file=/etc/postgresql/9.6/main/postgresql.conf"]
